using BusinessLayer.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using System.Linq;
using WebApiLayer.Models;
using WebApplication1.Controllers;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace ShipCost_NUnitTest
{
    [TestClass]
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [TestMethod]
        public void GetSipmentCost_ShouldReturnValidCostPrice()
        {
            var testProducts = GetTestProducts();
            var controller = new ShipmentCostController(null, testProducts);

            var result = controller.GetShipmentCost(testProducts);
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }

        private ShipmentCostModel GetTestProducts()
        {
            var testProducts = new ShipmentCostModel()
            {
                weightInKg = 10,
                heightInCm = 10,
                depth = 10,
                width = 10
            };
            return testProducts;
        }
    }
}