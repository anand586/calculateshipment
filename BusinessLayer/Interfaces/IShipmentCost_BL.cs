﻿using BusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.Interfaces
{
    public interface IShipmentCost_BL
    {
        string calculateCostOfShipment(ShipmentCostModel model);
    }
}
