﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.Classes
{
    public class CostByVolume : ShipCost
    {
        private readonly string _byType;
        private decimal _weightInKg;
        private decimal _heightInCm;
        private decimal _width;
        private decimal _depth;
        private decimal _value;

        public CostByVolume(decimal weightInKg, decimal heightInCm, decimal width, decimal depth)
        {
            _byType = "byVolume";
            _weightInKg = weightInKg;
            _heightInCm = heightInCm;
            _width = width;
            _depth = depth;
        }
        public override string byType
        {
            get { return _byType; }
        }

        public override decimal weightInKg
        {
            get { return _weightInKg; }
            set { _weightInKg = value; }
        }

        public override decimal heightInCm
        {
            get { return _heightInCm; }
            set { _heightInCm = value; }
        }

        public override decimal width
        {
            get { return _width; }
            set { _width = value; }
        }
        public override decimal depth
        {
            get { return _depth; }
            set { _depth = value; }
        }
        public override decimal value
        {
            get { return _value; }
            set { _value = value; }
        }
    }
}
