﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.Classes
{
   public abstract class ShipCost
    {
        public abstract string byType { get; }
        public abstract decimal weightInKg { get; set; }
        public abstract decimal heightInCm { get; set; }
        public abstract decimal width { get; set; }
        public abstract decimal depth { get; set; }
        public abstract decimal value { get; set; }
    }
}
