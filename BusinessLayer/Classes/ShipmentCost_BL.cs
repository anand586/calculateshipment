﻿using BusinessLayer.Interfaces;
using BusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.Classes
{
    public class ShipmentCost_BL : IShipmentCost_BL
    {
        public string calculateCostOfShipment(ShipmentCostModel model)
        {
            try
            {
                ShipCostFactory factoryCostShipment = null;
                string value = string.Empty;

                value = model.weightInKg < 10 ? "byVolume" : "byWeight";

                switch (value)
                {
                    case "byWeight":
                        factoryCostShipment = new CosyByWeightFactory(model.weightInKg, model.heightInCm, model.width, model.depth);
                        break;
                    case "byVolume":
                        factoryCostShipment = new CostByVolumeFactory(model.weightInKg, model.heightInCm, model.width, model.depth);
                        break;
                    default:
                        break;
                }

                return factoryCostShipment.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}
