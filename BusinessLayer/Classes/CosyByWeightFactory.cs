﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.Classes
{
    public class CosyByWeightFactory : ShipCostFactory
    {
        private decimal _weightInKg;
        private decimal _heightInCm;
        private decimal _width;
        private decimal _depth;

        public CosyByWeightFactory(decimal weightInKg, decimal heightInCm, decimal width, decimal depth)
        {
            _weightInKg = weightInKg;
            _heightInCm = heightInCm;
            _width = width;
            _depth = depth;
            GetShipmentCostDetails(_weightInKg, _heightInCm, _width, _depth);
        }
        public override string GetShipmentCostDetails(decimal weightInKg, decimal heightInCm, decimal width, decimal depth)
        {
            decimal costOfShipment = 0;
            string costApplicable = string.Empty;
            if (_weightInKg >= 10 && _weightInKg <= 50)
            {
                costOfShipment = 15 * _weightInKg;
                return costOfShipment.ToString();
            }
            else
            {
                costApplicable = "N/A";
                return costApplicable;
            }
        }
    }
}
