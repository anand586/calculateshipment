﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.Classes
{
    public class CostByVolumeFactory : ShipCostFactory
    {
        private decimal _weightInKg;
        private decimal _heightInCm;
        private decimal _width;
        private decimal _depth;

        public CostByVolumeFactory(decimal weightInKg, decimal heightInCm, decimal width, decimal depth)
        {
            _weightInKg = weightInKg;
            _heightInCm = heightInCm;
            _width = width;
            _depth = depth;
            GetShipmentCostDetails(_weightInKg, _heightInCm, _width, _depth);
        }
        public override string GetShipmentCostDetails(decimal weightInKg, decimal heightInCm, decimal width, decimal depth)
        {
            decimal volume = calculateVolume(_heightInCm, _width, _depth);
            decimal costOfShipment = 0;
            if (volume <= 1500)
            {
                costOfShipment = 0.05M * volume;
            }
            else if (volume >= 1500 && volume <= 2500)
            {
                costOfShipment = 0.04M * volume;
            }
            else
            {
                costOfShipment = 0.03M * volume;
            }
            return costOfShipment.ToString();
        }


        #region privateMethods
        private decimal calculateVolume(decimal heightInCm, decimal width, decimal depth)
        {
            decimal volume = (heightInCm * width * depth);

            return volume;
        }
        #endregion
    }
}