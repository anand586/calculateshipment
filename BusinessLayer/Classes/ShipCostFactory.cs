﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.Classes
{
    public abstract class ShipCostFactory
    {
        public abstract string GetShipmentCostDetails(decimal weightInKg, decimal heightInCm, decimal width, decimal depth);
    }
}
