﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.Models
{
    public class ShipmentCostModel
    {
        public decimal weightInKg { get; set; }
        public decimal heightInCm { get; set; }
        public decimal width { get; set; }
        public decimal depth { get; set; }
    }
}
