# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* The application calculates the shipment cost based on its weight and volume and provides the cost occured for shipment
1. The application consists of WebApi layer, Business layer and Unit test Test project majorly. which they interact with each other in order to get the results 
through Factory design Pattern for reusability and extensibility of future scope
2. Tested the app via NUnit Test framework
3. Error handlind is done in Http status codes api and # exception mechanism

* Version
Tech stacks used,
1. .Net core 3.0
2. WebApi 2.0
3. Factory design pattern
4. Dependency Injection
5. NUnit TDD

### How do I get set up? ###

* Summary of set up
1. Open in Visual studio 2019 to check the code and quality
2. Post man to check the GetCostOfShipment API Endpoint


* How to run tests
3. Run Test by clicking on Run All Tests in Test Explorer


### Who do I talk to? ###

* Repo owner or admin
Anand Kulkarni - 0415837157
