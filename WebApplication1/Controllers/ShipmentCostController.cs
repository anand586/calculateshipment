﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessLayer.Interfaces;
using BusinessLayer.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApiLayer.Models;

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ShipmentCostController : ControllerBase
    {
        private readonly IShipmentCost_BL _shipmentCost;
        private readonly ShipmentCostModel _shipmentCostModel;

        public ShipmentCostController(IShipmentCost_BL shipmentCost, ShipmentCostModel shipmentCostModel)
        {
            _shipmentCost = shipmentCost;
            _shipmentCostModel = shipmentCostModel;
        }

        // GET: api/ShipmentCost
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/ShipmentCost/5
        [HttpGet]
        [Route("GetCostOfShipment")]
        public string GetShipmentCost([FromBody]ShipmentCostModel model)
        {
            string value = string.Empty;

            value = _shipmentCost.calculateCostOfShipment(model).ToString();
            if (value != null)
            {
                return value;
            }
            else
            {
                return StatusCodes.Status404NotFound.ToString();
            }
        }

        // POST: api/ShipmentCost
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/ShipmentCost/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
