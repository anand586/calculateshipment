﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiLayer.Models
{
    public class ShipCostModel
    {
        public decimal weightInKg { get; set; }
        public decimal heightInCm { get; set; }
        public decimal width { get; set; }
        public decimal depth { get; set; }
    }
}
